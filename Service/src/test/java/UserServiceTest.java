import entity.Employee;
import entity.Role;
import entity.User;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import repository.UserRepository;
import service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserServiceImpl userService;

    @Test
    public void testGetAllUsers() throws Exception {
        Employee employee = mock(Employee.class);
        List<User> users = new ArrayList<>();
        users.add(new User(1, "login1", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(2, "login2", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(3, "login3", "password", "name", "surname", Role.ROLE_USER, employee));
        Page<User> usersFromRepository = new PageImpl<>(users);

        when(userRepository.findAll(Pageable.unpaged())).thenReturn(usersFromRepository);

        Page<User> usersPageFromService = userService.getAllUsers(Pageable.unpaged());

        assertEquals(usersFromRepository.getContent(), usersPageFromService.getContent());
        verify(userRepository, times(1)).findAll(Pageable.unpaged());

    }

    @Test
    public void testGetUserById() throws Exception {
        Employee employee = mock(Employee.class);
        User userFromRepository = new User(1, "login1",
                "password", "name", "surname", Role.ROLE_USER, employee);


        when(userRepository.getOne((long) 1)).thenReturn(userFromRepository);

        User userFromService = userService.getUserById((long) 1);
        assertEquals(userFromRepository, userFromService);
        verify(userRepository, times(1)).getOne((long) 1);
    }

    //    @Ignore
    @Test
    public void testCreateNewUser() throws Exception {
        Employee employeeMock = mock(Employee.class);
        User userFromRepository = new User(1, "login111",
                "password", "name", "surname", Role.ROLE_USER, employeeMock);
        Employee employee = new Employee("pos",
                "12@mail.ru", 123, 123, 123);

        verify(userRepository, times(0)).save(userFromRepository);
    }

    @Test
    public void testUpdateUser() {
        Employee employeeMock = new Employee("pos",
                "12@mail.ru", 123, 123, 123);
        User userToUpdate = new User(1, "login315",
                "password", "name", "surname", Role.ROLE_USER, employeeMock);
        User userBeforeUpdate = new User(1, "login111",
                "password", "name", "surname", Role.ROLE_USER, employeeMock);
        User userControl = new User(1, "login111",
                "password", "name", "surname", Role.ROLE_USER, employeeMock);

        when(userRepository.findById((long) 1)).thenReturn(Optional.of(userBeforeUpdate));
        when(userRepository.save(userToUpdate)).thenReturn(userToUpdate);


        User userServiceResponse = userService.updateUser((long) 1, userToUpdate);


        assertEquals(userBeforeUpdate, userToUpdate);
        assertEquals(userToUpdate, userServiceResponse);
        assertNotEquals(userBeforeUpdate, userControl);
        verify(userRepository, times(1)).findById((long) 1);
        verify(userRepository, times(1)).save(userToUpdate);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatUserUpdateThrowResourceNotFoundException() throws Exception {
        Employee employeeMock = new Employee("pos",
                "12@mail.ru", 123, 123, 123);
        User userToUpdate = new User(1, "login315",
                "password", "name", "surname", Role.ROLE_USER, employeeMock);
        userService.updateUser((long) 3, userToUpdate);

    }

    @Test(expected = InvalidInputException.class)
    public void testThatUserUpdateThrowInvalidInputException() throws Exception {
        Employee employee = spy(Employee.class);
        employee.setBalance(-123);
        employee.setSalaryPerHour(123);
        employee.setHoursPerMonth(12);
        User userToUpdate = new User(1, "login315",
                "password", "name", "surname", Role.ROLE_USER, employee);

        userService.updateUser((long) 1, userToUpdate);

    }

    @Test
    public void testDeleteUser() throws Exception {
        User userToDelete = spy(User.class);
        userToDelete.setId(1);

        when(userRepository.findById((long) 1))
                .thenReturn(Optional.ofNullable(userToDelete));

        userService.deleteUser((long) 1);
        verify(userRepository, times(1)).delete(userToDelete);
    }

    @Test
    public void testGetUserSubordinates() throws Exception {
        Employee employee = spy(Employee.class);
        employee.setHeadId(4);
        List<User> users = new ArrayList<>();
        users.add(new User(1, "login1", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(2, "login2", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(3, "login3", "password", "name", "surname", Role.ROLE_USER, employee));
        Page<User> usersFromRepository = new PageImpl<>(users);

        when(userRepository.findByHeadId(4, Pageable.unpaged())).thenReturn(usersFromRepository);

        Page<User> usersPageFromService = userService.getUserSubordinates(4, Pageable.unpaged());

        assertEquals(usersFromRepository.getContent(), usersPageFromService.getContent());
        verify(userRepository, times(1)).findByHeadId(4, Pageable.unpaged());
    }

    @Test
    public void testGetAllUserWithoutHead() throws Exception {
        Employee employee = spy(Employee.class);
        employee.setHeadId(0);
        List<User> users = new ArrayList<>();
        users.add(new User(1, "login1", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(2, "login2", "password", "name", "surname", Role.ROLE_USER, employee));
        users.add(new User(3, "login3", "password", "name", "surname", Role.ROLE_USER, employee));
        Page<User> usersFromRepository = new PageImpl<>(users);

        when(userRepository.getUsersWithoutHead(Pageable.unpaged())).thenReturn(usersFromRepository);

        Page<User> usersPageFromService = userService.getAllUsersWithoutHead(Pageable.unpaged());

        assertEquals(usersFromRepository.getContent(), usersPageFromService.getContent());
        verify(userRepository, times(1)).getUsersWithoutHead(Pageable.unpaged());
    }



}
