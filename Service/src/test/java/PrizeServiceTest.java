import entity.Fine;
import entity.Prize;
import entity.Role;
import entity.User;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import repository.PrizeRepository;
import repository.UserRepository;
import service.PrizeServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static net.bytebuddy.matcher.ElementMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PrizeServiceTest {

    @Mock
    PrizeRepository prizeRepository;

    @Mock
    UserRepository userRepository;

    @Captor
    ArgumentCaptor<Prize> prizeArgumentCaptor;

    @InjectMocks
    PrizeServiceImpl prizeService;

    @Test
    public void testGetAllPrizes() throws Exception {
        User user = mock(User.class);
        List<Prize> prizes = Mockito.spy(ArrayList.class);
        prizes.add(new Prize(1, "fbw1", 200, true, user));
        prizes.add(new Prize(2, "fbw2", 200, true, user));
        prizes.add(new Prize(3, "fbw3", 200, true, user));
        Page<Prize> prizesFromRepository = new PageImpl<>(prizes);

        when(prizeRepository.findAll(Pageable.unpaged())).thenReturn(prizesFromRepository);

        Page<Prize> prizesPageFromService = prizeService.getAllPrizes(Pageable.unpaged());

        assertEquals(prizesFromRepository.getContent(), prizesPageFromService.getContent());
        verify(prizeRepository, times(1)).findAll(Pageable.unpaged());

    }

    @Test
    public void testGetAllPrizesByUserId() throws Exception {

        List<Prize> prizes = new ArrayList<>();
        User user = Mockito.spy(User.class);
        user.setId(1);

        prizes.add(new Prize(1, "fbw1", 200, true, user));
        prizes.add(new Prize(2, "fbw2", 200, true, user));
        Page<Prize> prizesFromRepository = new PageImpl<>(prizes);

        when(prizeRepository.findByUserId((long) 1, Pageable.unpaged())).thenReturn(prizesFromRepository);
        Page<Prize> prizesFromService = prizeService.getAllPrizesByUserId((long) 1, Pageable.unpaged());

        assertEquals(prizesFromRepository.getContent(), prizesFromService.getContent());
        assertEquals(prizesFromRepository.getContent().get(0).getUser().getId(),
                prizesFromService.getContent().get(1).getUser().getId());

        verify(prizeRepository, times(1)).findByUserId((long) 1, Pageable.unpaged());
    }

    @Test
    public void testGetPrizeById() throws Exception {
        User user = mock(User.class);
        Prize prizeFromRepository = new Prize(1, "fbw", 200, true, user);

        when(prizeRepository.getOne((long) 1)).thenReturn(prizeFromRepository);

        Prize prizeFromService = prizeService.getPrizeById((long) 1);
        assertEquals(prizeFromRepository, prizeFromService);
        verify(prizeRepository, times(1)).getOne((long) 1);
    }

    @Test
    public void testCreateNewPrize() throws Exception {
        User user = mock(User.class);
        Prize prizeFromRepository = new Prize(1, "fbw", 200, true, user);

        when(prizeRepository.save(prizeFromRepository)).thenReturn(prizeFromRepository);
        when(userRepository.findById((long) 1)).thenReturn(java.util.Optional.of(user));


        Prize prizeFromService = prizeService.createPrize((long) 1, prizeFromRepository);

        assertEquals(prizeFromRepository, prizeFromService);
        verify(prizeRepository, times(1)).save(prizeFromRepository);
        verify(prizeRepository).save(prizeArgumentCaptor.capture());
        Prize prize = prizeArgumentCaptor.getValue();
        assertEquals(prize.getId(), prizeFromRepository.getId());
        assertEquals(prize.getCause(), prizeFromRepository.getCause());
    }

    @Test(expected = InvalidInputException.class)
    public void testThatPrizeCreateThrowException() throws Exception {
        Prize prizeFromRepository = new Prize(1, "fbw", -200, true, mock(User.class));

        prizeService.createPrize((long) 1, prizeFromRepository);
        verify(prizeRepository, times(1)).save(prizeFromRepository);
    }

    @Test
    public void testPrizeUpdate() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Prize prizeToUpdate = new Prize(1, "update", 200, true, user);
        Prize prizeBeforeUpdate = new Prize(1, "fgw", 200, true, user);
        Prize controlPrize = new Prize(1, "fgw", 200, true, user);

        when(prizeRepository.findById((long) 1)).thenReturn(java.util.Optional.of(prizeBeforeUpdate));
        when(prizeRepository.save(prizeToUpdate)).thenReturn(prizeToUpdate);
        when(userRepository.existsById((long) 1)).thenReturn(true);

        Prize prizeServiceResponse = prizeService.updatePrize((long) 1, (long) 1, prizeToUpdate);


        assertEquals(prizeBeforeUpdate, prizeToUpdate);
        assertEquals(prizeToUpdate, prizeServiceResponse);
        assertNotEquals(prizeBeforeUpdate, controlPrize);
        verify(prizeRepository, times(1)).findById((long) 1);
        verify(prizeRepository, times(1)).save(prizeToUpdate);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatPrizeUpdateThrowResourceNotFoundException() throws Exception {
        User user = spy(User.class);
        Prize prizeToUpdate = new Prize(1, "update", 200, true, user);
        prizeService.updatePrize(user.getId(), (long) 1, prizeToUpdate);

    }

    @Test(expected = InvalidInputException.class)
    public void testThatPrizeUpdateThrowInvalidInputException() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Prize prizeToUpdate = new Prize(1, "update", -200, true, user);

        when(userRepository.existsById((long) 1)).thenReturn(true);

        prizeService.updatePrize((long) 1, (long) 1, prizeToUpdate);

    }

    @Test
    public void testDeletePrize() throws Exception {
        Prize prizeToDelete = Mockito.spy(Prize.class);
        prizeToDelete.setId(1);

        when(prizeRepository.findByIdAndUserId((long) 1, prizeToDelete.getId()))
                .thenReturn(java.util.Optional.ofNullable(prizeToDelete));

        prizeService.deletePrize((long) 1, prizeToDelete.getId());
        verify(prizeRepository, times(1)).delete(prizeToDelete);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatDeletePrizeThrowResourceNotFoundException() throws Exception {
        Prize prizeToDelete = Mockito.spy(Prize.class);
        prizeToDelete.setId(1);

        prizeService.deletePrize((long) 2, prizeToDelete.getId());
    }

    @Test
    public void testGetUnapprovedFines() throws Exception {
        User user = spy(User.class);
        List<Prize> prizes = Mockito.spy(ArrayList.class);
        prizes.add(new Prize(1, "fbw1", 200, true,user));
        prizes.add(new Prize(2, "fbw2", 200, true,user));
        prizes.add(new Prize(3, "fbw3", 200, true,user));
        Page<Prize> prizesFromRepository = new PageImpl<>(prizes);

        when(prizeRepository.getUnapprovedPrizes(Pageable.unpaged())).thenReturn(prizesFromRepository);

        Page<Prize> prizesPageFromService = prizeService.getUnapprovedPrizes(Pageable.unpaged());

        assertEquals(prizesFromRepository.getContent(), prizesPageFromService.getContent());
        verify(prizeRepository, times(1)).getUnapprovedPrizes(Pageable.unpaged());
    }

    @Test
    public void testApprovePrize() throws Exception {

        User user = mock(User.class);
        Prize prizeToApprove = new Prize(1, "fbw", 200, false,user);

        when(prizeRepository.findById((long) 1)).thenReturn(java.util.Optional.of(prizeToApprove));
        when(prizeRepository.save(prizeToApprove)).thenReturn(prizeToApprove);
        when(userRepository.existsById((long) 1)).thenReturn(true);

        Prize prize = prizeService.approvePrize((long)1,(long)1);

        assertEquals(true,prize.isApproved());
        verify(prizeRepository,times(1)).save(prizeToApprove);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatApprovePrizeThrowResourceNotFoundException() throws Exception {
        Prize prizeToDelete = Mockito.spy(Prize.class);
        prizeToDelete.setId(1);

        prizeService.approvePrize((long) 1,prizeToDelete.getId());
    }
}
