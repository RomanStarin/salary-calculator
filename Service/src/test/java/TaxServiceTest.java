import entity.Tax;
import entity.User;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import repository.PrizeRepository;
import repository.TaxRepository;
import repository.UserRepository;
import service.PrizeServiceImpl;
import service.TaxServiceImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TaxServiceTest {

    @Mock
    TaxRepository taxRepository;

    @Mock
    UserRepository userRepository;

    @InjectMocks
    TaxServiceImpl taxService;

    @Test
    public void testGetAllTaxes() throws Exception {
        User user = mock(User.class);
        List<Tax> taxes = Mockito.spy(ArrayList.class);
        taxes.add(new Tax(1, "tax1", 20, user));
        taxes.add(new Tax(2, "tax2", 20, user));
        taxes.add(new Tax(3, "tax3", 20, user));
        Page<Tax> taxesFromRepository = new PageImpl<>(taxes);

        when(taxRepository.findAll(Pageable.unpaged())).thenReturn(taxesFromRepository);

        Page<Tax> taxesPageFromService = taxService.getAllTaxes(Pageable.unpaged());

        assertEquals(taxesFromRepository.getContent(), taxesPageFromService.getContent());
        verify(taxRepository, times(1)).findAll(Pageable.unpaged());

    }

    @Test
    public void testGetAllTaxesByUserId() throws Exception {

        List<Tax> taxes = new ArrayList<>();
        User user = spy(User.class);
        user.setId(1);

        taxes.add(new Tax(1, "tax1", 20, user));
        taxes.add(new Tax(2, "tax2", 20, user));
        Page<Tax> taxesFromRepository = new PageImpl<>(taxes);

        when(taxRepository.findByUserId((long) 1, Pageable.unpaged())).thenReturn(taxesFromRepository);
        Page<Tax> taxesFromService = taxService.getAllTaxesByUserId((long) 1, Pageable.unpaged());

        assertEquals(taxesFromRepository.getContent(), taxesFromService.getContent());
        assertEquals(taxesFromRepository.getContent().get(0).getUser().getId(),
                taxesFromService.getContent().get(1).getUser().getId());

        verify(taxRepository, times(1)).findByUserId((long) 1, Pageable.unpaged());
    }

    @Test
    public void testGetTaxById() throws Exception {
        User user = mock(User.class);
        Tax taxFromRepository = new Tax(1, "fbw", 20, user);

        when(taxRepository.getOne((long) 1)).thenReturn(taxFromRepository);

        Tax prizeFromService = taxService.getTaxById((long) 1);
        assertEquals(taxFromRepository, prizeFromService);
        verify(taxRepository, times(1)).getOne((long) 1);
    }

    @Test
    public void testCreateNewPrize() throws Exception {
        User user = mock(User.class);
        Tax taxFromRepository = new Tax(1, "fbw", 20, user);

        when(taxRepository.save(taxFromRepository)).thenReturn(taxFromRepository);
        when(userRepository.findById((long) 1)).thenReturn(Optional.of(user));


        Tax taxFromService = taxService.createTax((long) 1, taxFromRepository);

        assertEquals(taxFromRepository, taxFromService);
        verify(taxRepository, times(1)).save(taxFromRepository);
    }

    @Test(expected = InvalidInputException.class)
    public void testThatTaxCreateThrowException() throws Exception {
        Tax taxFromRepository = new Tax(1, "fbw", -20, mock(User.class));

        taxService.createTax((long) 1, taxFromRepository);
        verify(taxRepository, times(1)).save(taxFromRepository);
    }

    @Test
    public void testPrizeUpdate() throws Exception {
        User user = spy(User.class);
        user.setId(1);
        Tax taxToUpdate = new Tax(1, "update", 20, user);
        Tax taxBeforeUpdate = new Tax(1, "fgw", 20, user);
        Tax controlTax = new Tax(1, "fgw", 20, user);

        when(taxRepository.findById((long) 1)).thenReturn(Optional.of(taxBeforeUpdate));
        when(taxRepository.save(taxToUpdate)).thenReturn(taxToUpdate);
        when(userRepository.existsById((long) 1)).thenReturn(true);

        Tax taxServiceResponse = taxService.updateTax((long) 1, (long) 1, taxToUpdate);


        assertEquals(taxBeforeUpdate, taxToUpdate);
        assertEquals(taxToUpdate, taxServiceResponse);
        assertNotEquals(taxBeforeUpdate, controlTax);
        verify(taxRepository, times(1)).findById((long) 1);
        verify(taxRepository, times(1)).save(taxToUpdate);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatTaxUpdateThrowResourceNotFoundException() throws Exception {
        User user = spy(User.class);
        Tax taxToUpdate = new Tax(1, "update", 20, user);
        taxService.updateTax(user.getId(), (long) 1, taxToUpdate);

    }

    @Test(expected = InvalidInputException.class)
    public void testThatTaxUpdateThrowInvalidInputException() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Tax taxToUpdate = new Tax(1, "update", -20, user);

        when(userRepository.existsById((long) 1)).thenReturn(true);

        taxService.updateTax((long) 1, (long) 1, taxToUpdate);

    }

    @Test
    public void testDeleteTax() throws Exception {
        Tax taxToDelete = spy(Tax.class);
        taxToDelete.setId(1);

        when(taxRepository.findByIdAndUserId((long) 1, taxToDelete.getId()))
                .thenReturn(Optional.ofNullable(taxToDelete));

        taxService.deleteTax((long) 1, taxToDelete.getId());
        verify(taxRepository, times(1)).delete(taxToDelete);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatDeleteTaxThrowResourceNotFoundException() throws Exception {
        Tax taxToDelete = Mockito.spy(Tax.class);
        taxToDelete.setId(1);

        taxService.deleteTax((long) 2, taxToDelete.getId());
    }

    @Test
    public void testGetListAllTaxesByUserId() throws Exception {
        List<Tax> taxes = new ArrayList<>();
        User user = spy(User.class);
        user.setId(1);

        taxes.add(new Tax(1, "tax1", 20, user));
        taxes.add(new Tax(2, "tax2", 20, user));

        when(taxRepository.findByUserId((long) 1)).thenReturn(taxes);
        List<Tax> taxesFromService = taxService.getAllTaxesByUserId((long) 1);

        assertEquals(taxes, taxesFromService);
        assertEquals(taxes.get(0).getUser().getId(),
                taxesFromService.get(1).getUser().getId());

        verify(taxRepository, times(1)).findByUserId((long) 1);
    }
}
