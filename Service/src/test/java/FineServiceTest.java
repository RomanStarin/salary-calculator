import entity.Fine;
import entity.Role;
import entity.User;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.web.servlet.MockMvc;
import repository.FineRepository;
import repository.UserRepository;
import service.FineServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FineServiceTest {


    @Mock
    private FineRepository fineRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private FineServiceImpl fineService;



    @Test
    public void testGetFine() throws Exception {
        Fine fineFromRepository = new Fine(1, "fbw", 200, true);

        when(fineRepository.getOne((long) 1)).thenReturn(fineFromRepository);

        Fine fineFromService = fineService.getFineById((long) 1);
        assertEquals(fineFromRepository, fineFromService);
    }

    @Test
    public void testThatFineCreate() throws Exception {
        Fine fineFromRepository = new Fine(1, "fbw", 200, true);
        User user = new User("test", "test", "test", "test", Role.ROLE_USER);

        when(fineRepository.save(fineFromRepository)).thenReturn(fineFromRepository);
        when(userRepository.findById((long) 1)).thenReturn(java.util.Optional.of(user));


        Fine fineFromService = fineService.createFine((long) 1, fineFromRepository);

        assertEquals(fineFromRepository, fineFromService);
    }

    @Test(expected = InvalidInputException.class)
    public void testThatFineCreateThrowException() throws Exception {
        Fine fineFromRepository = new Fine(1, "fbw", -200, true);

        fineService.createFine((long) 1, fineFromRepository);
    }

    @Test
    public void testThatGetAllFines() throws Exception {
        List<Fine> fines = Mockito.spy(ArrayList.class);
        fines.add(new Fine(1, "fbw1", 200, true));
        fines.add(new Fine(2, "fbw2", 200, true));
        fines.add(new Fine(3, "fbw3", 200, true));
        Page<Fine> finesFromRepository = new PageImpl<>(fines);

        when(fineRepository.findAll(Pageable.unpaged())).thenReturn(finesFromRepository);

        Page<Fine> finesPageFromService = fineService.getAllFines(Pageable.unpaged());

        assertEquals(finesFromRepository.getContent(), finesPageFromService.getContent());
    }

    @Test
    public void testThatGetAllUserFines() throws Exception {
        List<Fine> fines = new ArrayList<>();
        User user = new User();
        user.setId(1);

        fines.add(new Fine(1, "fbw1", 200, true, user));
        fines.add(new Fine(2, "fbw2", 200, true, user));
        Page<Fine> finesFromRepository = new PageImpl<>(fines);

        when(fineRepository.findByUserId((long) 1, Pageable.unpaged())).thenReturn(finesFromRepository);
        Page<Fine> finesFromService = fineService.getAllFinesByUserId((long) 1, Pageable.unpaged());

        assertEquals(finesFromRepository.getContent(), finesFromService.getContent());
        assertEquals(finesFromRepository.getContent().get(0).getUser().getId(), finesFromService.getContent().get(1).getUser().getId());
    }

    @Test
    public void testFineUpdate() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Fine fineToUpdate = new Fine(1, "update", 200, true, user);
        Fine fineBeforeUpdate = new Fine(1, "fbw", 200, true, user);
        Fine controlFine = new Fine(1, "fbw", 200, true, user);

        when(fineRepository.findById((long) 1)).thenReturn(java.util.Optional.of(fineBeforeUpdate));
        when(fineRepository.save(fineToUpdate)).thenReturn(fineToUpdate);
        when(userRepository.existsById((long) 1)).thenReturn(true);

        Fine fineServiceResponse = fineService.updateFine((long) 1, (long) 1, fineToUpdate);


        assertEquals(fineBeforeUpdate, fineToUpdate);
        assertEquals(fineToUpdate, fineServiceResponse);
        assertNotEquals(fineBeforeUpdate, controlFine);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatFineUpdateThrowResourceNotFoundException() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Fine fineToUpdate = new Fine(1, "update", 200, true, user);

         fineService.updateFine((long) 2, (long) 1, fineToUpdate);

    }


    @Test(expected = InvalidInputException.class)
    public void testThatFineUpdateThrowInvalidInputException() throws Exception {
        User user = Mockito.spy(User.class);
        user.setId(1);
        Fine fineToUpdate = new Fine(1, "update", -200, true, user);

        when(userRepository.existsById((long) 1)).thenReturn(true);

        fineService.updateFine((long) 1, (long) 1, fineToUpdate);

    }

    @Test
    public void testDeleteFine() throws Exception {
        Fine fineToDelete = Mockito.spy(Fine.class);
        fineToDelete.setId(1);

        when(fineRepository.findByIdAndUserId((long) 1, fineToDelete.getId()))
                .thenReturn(java.util.Optional.ofNullable(fineToDelete));

        fineService.deleteFine((long) 1, fineToDelete.getId());
        verify(fineRepository, times(1)).delete(fineToDelete);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatDeleteFineThrowResourceNotFoundException() throws Exception {
        Fine fineToDelete = Mockito.spy(Fine.class);
        fineToDelete.setId(1);

        fineService.deleteFine((long) 2, fineToDelete.getId());
    }

    @Test
    public void testGetUnapprovedFines() throws Exception {
        List<Fine> fines = Mockito.spy(ArrayList.class);
        fines.add(new Fine(1, "fbw1", 200, true));
        fines.add(new Fine(2, "fbw2", 200, true));
        fines.add(new Fine(3, "fbw3", 200, true));
        Page<Fine> finesFromRepository = new PageImpl<>(fines);

        when(fineRepository.getUnapprovedFines(Pageable.unpaged())).thenReturn(finesFromRepository);

        Page<Fine> finesPageFromService = fineService.getUnapprovedFines(Pageable.unpaged());

        assertEquals(finesFromRepository.getContent(), finesPageFromService.getContent());
        verify(fineRepository, times(1)).getUnapprovedFines(Pageable.unpaged());
    }

    @Test
    public void testApproveFine() throws Exception {

        Fine fineToApprove = new Fine(1, "fbw", 200, false);

        when(fineRepository.findById((long) 1)).thenReturn(java.util.Optional.of(fineToApprove));
        when(fineRepository.save(fineToApprove)).thenReturn(fineToApprove);
        when(userRepository.existsById((long) 1)).thenReturn(true);

        Fine fine = fineService.approveFine((long)1,(long)1);

        assertEquals(true,fine.isApproved());
        verify(fineRepository,times(1)).save(fineToApprove);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void testThatApproveFineThrowResourceNotFoundException() throws Exception {
        Fine fineToDelete = Mockito.spy(Fine.class);
        fineToDelete.setId(1);

        fineService.approveFine((long) 1,fineToDelete.getId());
    }

}
