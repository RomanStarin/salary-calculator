package utils;


import entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import service.FineService;
import service.PrizeService;
import service.TaxService;

import java.util.concurrent.atomic.AtomicReference;

@Component
public class SalaryCalculation {

    @Autowired
    private PrizeService prizeService;

    @Autowired
    private TaxService taxService;

    @Autowired
    private FineService fineService;

    public Salary calculateSalary(User user) {
        Salary userSalary = new Salary();
        double salaryBeforeTaxes = user.getEmployee().getSalaryPerHour() * user.getEmployee().getHoursPerMonth();
        userSalary.setSalaryBeforeTaxes(salaryBeforeTaxes);
        double taxesAmount = calculateTaxes(user, salaryBeforeTaxes);
        double prizesAmount = calculatePrizes(user);
        double finesAmount = calculateFines(user);
        userSalary.setPrizesAmount(prizesAmount);
        userSalary.setFinesAmount(finesAmount);
        userSalary.setTaxesAmount(taxesAmount);

        salaryBeforeTaxes += prizesAmount - finesAmount;
        double salaryAfterTaxes = salaryBeforeTaxes + taxesAmount;

        userSalary.setSalaryAfterTaxes(salaryAfterTaxes);

        return userSalary;
    }

    public double calculatePrizes(User user) {
        double prizesAmount = prizeService.getAllPrizesByUserId(user.getId(), Pageable.unpaged())
                .stream()
                .filter(Prize::isApproved)
                .mapToDouble(Prize::getAmount).sum();

        return prizesAmount;
    }

    public double calculateFines(User user) {
        user.getClass();
        return fineService.getAllFinesByUserId(user.getId(), Pageable.unpaged())
                .stream()
                .filter(Fine::isApproved)
                .mapToDouble(Fine::getAmount).sum();
    }

    public double calculateTaxes(User user, double salaryBeforeTaxes) {

        AtomicReference<Double> taxesAmount = new AtomicReference<>((double) 0);

        if (taxService.getAllTaxesByUserId(user.getId()).size() != 0) {
            taxService.getAllTaxesByUserId(user.getId(), Pageable.unpaged())
                    .stream()
                    .forEach(tax -> {
                taxesAmount.updateAndGet(v -> v - salaryBeforeTaxes * tax.getPercent() / 100);
            });

            return taxesAmount.get();
        } else return 0;
    }
}
