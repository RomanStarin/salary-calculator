package utils;

import controllers.TaxController;
import entity.*;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import service.*;

@Component
public class EmailSender {

    @Autowired
    private static final Logger logger = LogManager.getLogger(TaxController.class);

    @Autowired
    private PrizeService prizeService;

    @Autowired
    private TaxService taxService;

    @Autowired
    private FineService fineService;

    public void sendMessageToEmail(String recipientEmail, String message) throws EmailException {
        Email email = new SimpleEmail();
        try{
            email.setHostName("smtp.gmail.com");
            email.setSmtpPort(465);
            email.setAuthenticator(new DefaultAuthenticator("salarycalculatorsupp", "ytpf,snmgfhjkm"));
            email.setSSLOnConnect(true);
            email.setFrom("salarycalculatorsupp@gmail.com");
            email.setSubject("salary report");
            email.setMsg(message);
            email.addTo(recipientEmail);
            email.send();
            logger.info("Message sent to user");
        }
        catch (EmailException e){
            logger.error(e.getMessage());
        }
    }

    public  String prepareSalaryReport(User user, Salary salary) {
        String report;
        String prizes = "";
        String fines = "";
        String taxes = "";

        for (Prize prize : prizeService.getAllPrizesByUserId(user.getId(), Pageable.unpaged())) {
            prizes += prize.toString();
        }

        for (Fine fine : fineService.getAllFinesByUserId(user.getId(), Pageable.unpaged())) {
            fines += fine.toString();
        }

        for (Tax tax : taxService.getAllTaxesByUserId(user.getId(),Pageable.unpaged())) {
            taxes += tax.toString();
        }

        report = user.getName() + " " + user.getSurname() + " salary report:\n" +
                "Hours worked: " + user.getEmployee().getHoursPerMonth() + "\n" +
                "Salary per hour: " + user.getEmployee().getSalaryPerHour() + "\n" +
                "---------------------------------------------" + "\n" +
                "Salary before taxes: " + salary.getSalaryBeforeTaxes() + "\n" +
                "---------------------------------------------" + "\n" + "\n" +
                "Prizes: " + "\n" +
                prizes + "\n" +
                "---------------------------------------------" + "\n" +
                "Prizes amount: +" + salary.getPrizesAmount() + "\n" +
                "Fines: " + "\n" +
                fines + "\n" +
                "Fines amount: -" + salary.getFinesAmount() + "\n" +
                "---------------------------------------------" + "\n" +
                "Taxes: " + "\n" +
                taxes + "\n" +
                "Taxes amount: " + salary.getTaxesAmount() + "\n" +
                "---------------------------------------------" + "\n" +
                "To accrual: " + salary.getSalaryAfterTaxes() + "\n" +
                "Your balance: " + user.getEmployee().getBalance() + "\n";

        logger.info("salary report has been prepared ");
        return report;


    }
}
