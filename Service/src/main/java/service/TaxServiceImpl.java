package service;

import entity.Tax;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.TaxRepository;
import repository.UserRepository;

import java.util.List;

@Service
public class TaxServiceImpl implements TaxService {

    @Autowired
    TaxRepository taxRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<Tax> getAllTaxes(Pageable pageable) {
        return taxRepository.findAll(pageable);
    }

    @Override
    public Page<Tax> getAllTaxesByUserId(Long userId, Pageable pageable) {
        return taxRepository.findByUserId(userId, pageable);
    }

    @Override
    public List<Tax> getAllTaxesByUserId(Long userId) {
        return taxRepository.findByUserId(userId);
    }

    @Override
    public Tax getTaxById(Long id) {
        return taxRepository.getOne(id);
    }

    @Override
    public Tax createTax(Long userId, Tax tax) {
        if (isTaxValid(tax)) {
            return userRepository.findById(userId).map(user -> {
                tax.setUser(user);
                return taxRepository.save(tax);
            }).orElseThrow(() -> new ResourceNotFoundException("User id " + userId + " not found"));
        } else throw new InvalidInputException("Percent is negative");
    }

    @Override
    public Tax updateTax(Long userId, Long taxId, Tax taxToUpdate) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }

        if (isTaxValid(taxToUpdate)) {
            return taxRepository.findById(taxId).map(tax -> {
                tax.setName(taxToUpdate.getName());
                tax.setPercent(taxToUpdate.getPercent());
                return taxRepository.save(tax);
            }).orElseThrow(() -> new ResourceNotFoundException("Tax id " + taxId + " not found"));
        } else throw new InvalidInputException("Percent is negative");
    }

    @Override
    public ResponseEntity<?> deleteTax(Long userId, Long taxId) {
        return taxRepository.findByIdAndUserId(taxId, userId).map(tax -> {
            taxRepository.delete(tax);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Tax not found with id " + taxId + " and userId " + userId));
    }

    @Override
    public boolean isTaxValid(Tax tax) {
        if (tax.getPercent() > 0) {
            return true;
        }
        return false;
    }
}
