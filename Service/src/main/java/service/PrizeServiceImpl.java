package service;

import entity.Fine;
import entity.Prize;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.PrizeRepository;
import repository.UserRepository;

@Service
public class PrizeServiceImpl implements PrizeService {

    @Autowired
    PrizeRepository prizeRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<Prize> getAllPrizes(Pageable pageable) {
        return prizeRepository.findAll(pageable);
    }

    @Override
    public Page<Prize> getAllPrizesByUserId(Long userId, Pageable pageable) {
        return prizeRepository.findByUserId(userId, pageable);
    }

    @Override
    public Prize getPrizeById(Long id) {
        return prizeRepository.getOne(id);
    }

    @Override
    public Prize createPrize(Long userId, Prize prize) {
        if (isPrizeValid(prize)) {
            return userRepository.findById(userId).map(user -> {
                prize.setUser(user);
                return prizeRepository.save(prize);
            }).orElseThrow(() -> new ResourceNotFoundException("User id " + userId + " not found"));
        } else throw new InvalidInputException("Prize amount is negative");
    }

    @Override
    public Prize updatePrize(Long userId, Long prizeId, Prize prizeToUpdate) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }

        if (isPrizeValid(prizeToUpdate)) {
            return prizeRepository.findById(prizeId).map(prize -> {
                prize.setCause(prizeToUpdate.getCause());
                prize.setAmount(prizeToUpdate.getAmount());
                prize.setApproved(prizeToUpdate.isApproved());
                return prizeRepository.save(prize);
            }).orElseThrow(() -> new ResourceNotFoundException("Prize id " + prizeId + " not found"));
        }
        else throw new InvalidInputException("Prize amount is negative");
    }

    @Override
    public ResponseEntity<?> deletePrize(Long userId, Long prizeId) {
        return prizeRepository.findByIdAndUserId(prizeId, userId).map(prize -> {
            prizeRepository.delete(prize);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Prize not found with id " + prizeId + " and userId " + userId));
    }

    @Override
    public Page<Prize> getUnapprovedPrizes(Pageable pageable) {
        return prizeRepository.getUnapprovedPrizes(pageable);
    }

    @Override
    public Prize approvePrize(Long userId, Long prizeId) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }

        return prizeRepository.findById(prizeId).map(prize -> {
            prize.setApproved(true);
            return prizeRepository.save(prize);
        }).orElseThrow(() -> new ResourceNotFoundException("Prize id " + prizeId + " not found"));
    }

    @Override
    public boolean isPrizeValid(Prize prize) {
        if (prize.getAmount() > 0) {
            return true;
        }
        return false;
    }
}
