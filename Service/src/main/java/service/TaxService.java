package service;

import entity.Tax;
import entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface TaxService {

    Page<Tax> getAllTaxes(Pageable pageable);

    Page<Tax> getAllTaxesByUserId(Long userId, Pageable pageable);

    List<Tax> getAllTaxesByUserId(Long userId);

    Tax getTaxById(Long id);

    Tax createTax(Long userId,Tax tax);

    Tax updateTax(Long userId,Long taxId, Tax taxToUpdate);

    ResponseEntity<?> deleteTax(Long userId, Long taxId);

    boolean isTaxValid(Tax tax);
}
