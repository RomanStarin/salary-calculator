package service;

import entity.Fine;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import repository.FineRepository;
import repository.UserRepository;

@Service
public class FineServiceImpl implements FineService {

    @Autowired
    FineRepository fineRepository;

    @Autowired
    UserRepository userRepository;

    @Override
    public Page<Fine> getAllFines(Pageable pageable) {
        return fineRepository.findAll(pageable);
    }

    @Override
    public Page<Fine> getAllFinesByUserId(Long userId, Pageable pageable) {
        return fineRepository.findByUserId(userId, pageable);
    }

    @Override
    public Fine getFineById(Long id) {
        return fineRepository.getOne(id);
    }

    @Override
    public Fine createFine(Long userId, Fine fine) {
        if (isFineValid(fine)) {
            return userRepository.findById(userId).map(user -> {
                fine.setUser(user);
                return fineRepository.save(fine);
            }).orElseThrow(() -> new ResourceNotFoundException("User id " + userId + " not found"));
        } else throw new InvalidInputException("Fine amount is negative");
    }

    @Override
    public Fine updateFine(Long userId, Long fineId, Fine fineToUpdate) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }

        if (isFineValid(fineToUpdate)) {
            return fineRepository.findById(fineId).map(fine -> {
                fine.setCause(fineToUpdate.getCause());
                fine.setAmount(fineToUpdate.getAmount());
                fine.setApproved(fineToUpdate.isApproved());
                return fineRepository.save(fine);
            }).orElseThrow(() -> new ResourceNotFoundException("Fine id " + fineId + " not found"));
        } else throw new InvalidInputException("Negative amount");
    }

    @Override
    public ResponseEntity<?> deleteFine(Long userId, Long fineId) {
        return fineRepository.findByIdAndUserId(fineId, userId).map(fine -> {
            fineRepository.delete(fine);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Fine not found with id " + fineId + " and userId " + userId));
    }

    @Override
    public Page<Fine> getUnapprovedFines(Pageable pageable) {
        return fineRepository.getUnapprovedFines(pageable);
    }

    @Override
    public Fine approveFine(Long userId, Long fineId) {
        if (!userRepository.existsById(userId)) {
            throw new ResourceNotFoundException("User id " + userId + " not found");
        }

        return fineRepository.findById(fineId).map(fine -> {
            fine.setApproved(true);
            return fineRepository.save(fine);
        }).orElseThrow(() -> new ResourceNotFoundException("Fine id " + fineId + " not found"));
    }

    @Override
    public boolean isFineValid(Fine fine) {
        if (fine.getAmount() > 0) {
            return true;
        }
        return false;
    }
}
