package service;

import entity.Fine;
import entity.Prize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface PrizeService {

    Page<Prize> getAllPrizes(Pageable pageable);

    Page<Prize> getAllPrizesByUserId(Long userId, Pageable pageable);

    Prize getPrizeById(Long id);

    Prize createPrize(Long userId,Prize prize);

    Prize updatePrize(Long userId,Long prizeId, Prize prizeToUpdate);

    ResponseEntity<?> deletePrize(Long userId, Long prizeId);

    Page<Prize> getUnapprovedPrizes(Pageable pageable);

    Prize approvePrize(Long userId, Long prizeId);

    boolean isPrizeValid(Prize prize);
}
