package service;

import entity.Fine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface FineService {

    Page<Fine> getAllFines(Pageable pageable);

    Page<Fine> getAllFinesByUserId(Long userId, Pageable pageable);

    Fine getFineById(Long id);

    Fine createFine(Long userId,Fine fine);

    Fine updateFine(Long userId,Long fineId, Fine fineToUpdate);

    ResponseEntity<?> deleteFine(Long userId, Long fineId);

    Page<Fine> getUnapprovedFines(Pageable pageable);

    Fine approveFine(Long userId, Long fineId);

    boolean isFineValid(Fine fine);

}
