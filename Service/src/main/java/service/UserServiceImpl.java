package service;

import entity.Employee;
import entity.Role;
import entity.Salary;
import entity.User;
import exception.IllegalAccessException;
import exception.InvalidInputException;
import exception.ResourceNotFoundException;
import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import repository.UserRepository;
import utils.EmailSender;
import utils.SalaryCalculation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TaxService taxService;

    @Autowired
    private SalaryCalculation salaryCalculation;

    @Autowired
    private EmailSender emailSender;


    @Override
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public User createUser(User user, Employee employee) {
        user.setEmployee(employee);
        employee.setUser(user);
        if (isUserValid(user) && isLoginValid(user.getLogin()) && isPasswordValid(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
            return user;
        } else throw new InvalidInputException("Login or password is to short");
    }

    @Override
    public User updateUser(Long id, User userToUpdate) {
        if (isUserValid(userToUpdate) && isLoginValid(userToUpdate.getLogin())) {
            return userRepository.findById(id).map(user -> {
                user.setLogin(userToUpdate.getLogin());
                user.setName(userToUpdate.getName());
                user.setSurname(userToUpdate.getSurname());
                user.setRole(userToUpdate.getRole());
                user.getEmployee().setPosition(userToUpdate.getEmployee().getPosition());
                user.getEmployee().setEmail(userToUpdate.getEmployee().getEmail());
                user.getEmployee().setSalaryPerHour(userToUpdate.getEmployee().getSalaryPerHour());
                user.getEmployee().setBalance(userToUpdate.getEmployee().getBalance());
                user.getEmployee().setHoursPerMonth(userToUpdate.getEmployee().getHoursPerMonth());
                user.getEmployee().setHeadId(userToUpdate.getEmployee().getHeadId());
                return userRepository.save(user);
            }).orElseThrow(() -> new ResourceNotFoundException("UserId " + id + "not found"));
        } else throw new InvalidInputException("Login or password is to short");
    }


    @Override
    public ResponseEntity<?> deleteUser(Long userId) {
        return userRepository.findById(userId).map(user -> {
            userRepository.delete(user);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("UserId " + userId + "not found"));
    }

    @Override
    public Page<User> getUserSubordinates(Integer userId, Pageable pageable) {
        if (isUserRequestValid(userId.longValue())){
        return userRepository.findByHeadId(userId, pageable);}
        else throw new IllegalAccessException("You don't have access to fulfill this request");
    }

    @Override
    public Page<User> getAllUsersWithoutHead(Pageable pageable) {
        return userRepository.getUsersWithoutHead(pageable);
    }

    @Override
    public User addSubordinateToUser(Long userId, Long subordinateId) {
        User user = getUserById(subordinateId);
        user.getEmployee().setHeadId(Math.toIntExact(userId));
        return updateUser(subordinateId, user);
    }

    @Override
    public User deleteSubordinateFromUser(Long userId, Long subordinateId) {
        User user = getUserById(subordinateId);
        user.getEmployee().setHeadId(0);
        return updateUser(subordinateId, user);
    }

    @Override
    public User setHoursForSubordinate(Long userId, Long subordinateId, Integer hours) {
        if(isUserRequestValid(userId.longValue())){
        if (hours > 0) {
            User user = getUserById(subordinateId);
            user.getEmployee().setHoursPerMonth(hours);
            return updateUser(subordinateId, user);
        } else throw new InvalidInputException("Hours is negative");}
        else throw new IllegalAccessException("You don't have access to fulfill this request");
    }

    @Override
    public User changeUserPassword(Long userId, String newPassword) {
        if(isUserRequestValid(userId.longValue())) {
            if (isPasswordValid(newPassword)) {
                User user = getUserById(userId);
                user.setPassword(passwordEncoder.encode(newPassword));
                return updatePassword(userId, user);
            } else throw new InvalidInputException("Password is too short");
        }
        else throw new IllegalAccessException("You don't have access to fulfill this request");
    }

    @Override
    public User changeUserLogin(Long userId, String newLogin) {
        if (isUserRequestValid(userId.longValue())){
        if (isLoginValid(newLogin)) {
            User user = getUserById(userId);
            user.setLogin(newLogin);
            return updateUser(userId, user);
        } else throw new InvalidInputException("Login is too short");}
        else throw new IllegalAccessException("You don't have access to fulfill this request");
    }

    @Override
    public User calculateSalary(Long userId) {
        Salary salary = salaryCalculation.calculateSalary(getUserById(userId));
        User user = getUserById(userId);
        String report = emailSender.prepareSalaryReport(user, salary);
        try {
            emailSender.sendMessageToEmail(user.getEmployee().getEmail(), report);
        } catch (EmailException e) {
            e.printStackTrace();
        }
        user.getEmployee().setBalance(salary.getSalaryAfterTaxes());
        user.getEmployee().setHoursPerMonth(0);
        return updateUser(userId, user);
    }

    @Override
    public User updatePassword(Long id, User userToUpdate) {
        if (isPasswordValid(userToUpdate.getPassword())) {
            return userRepository.findById(id).map(user -> {
                user.setPassword(userToUpdate.getPassword());
                return userRepository.save(user);
            }).orElseThrow(() -> new ResourceNotFoundException("UserId " + id + "not found"));
        } else throw new InvalidInputException("Login or password is to short");
    }

    @Override
    public boolean isUserValid(User user) {
        if (user.getEmployee().getSalaryPerHour() > 0
                && user.getEmployee().getBalance() >= 0
                && user.getEmployee().getHoursPerMonth() >= 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isPasswordValid(String password) {
        if (password.toCharArray().length < 8) {
            return false;
        } else return true;
    }

    @Override
    public boolean isLoginValid(String login) {
        if (login.toCharArray().length < 8) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isUserRequestValid(Long userId) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            if (userRepository.findByLogin(((UserDetails) principal).getUsername()).getId() == userId ||
            userRepository.findByLogin(((UserDetails) principal).getUsername()).getRole() == Role.ROLE_HR)
                return true;
        } else {
            throw new ResourceNotFoundException("User with id" + userId + "not found");
        }
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userRepository.findByLogin(login);
        List<Role> roles = new ArrayList<>();
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password");
        }
        roles.add(user.getRole());
        return new org.springframework.security.core.userdetails.User(user.getLogin(),
                user.getPassword(),
                mapRolesToAuthorities(roles));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Role role : roles) {
            authorities.add(new SimpleGrantedAuthority(role.toString()));
        }
        return authorities;
    }
}
