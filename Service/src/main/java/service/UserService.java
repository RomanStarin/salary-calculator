package service;

import entity.Employee;
import entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface UserService{

    Page<User> getAllUsers(Pageable pageable);

    User getUserById(Long id);

    User createUser(User user, Employee employee);

    User updateUser(Long id, User user);

    ResponseEntity<?> deleteUser(Long userId);

    Page<User> getUserSubordinates(Integer userId, Pageable pageable);

    Page<User> getAllUsersWithoutHead( Pageable pageable);

    User addSubordinateToUser(Long userId, Long subordinateId);

    User deleteSubordinateFromUser(Long userId, Long subordinateId);

    User setHoursForSubordinate(Long userId, Long subordinateId, Integer hours);

    User changeUserPassword(Long userId, String newPassword);

    User changeUserLogin(Long userId, String newLogin);

    User calculateSalary(Long userId);

    User updatePassword(Long id, User user);

    boolean isUserValid(User user);

    boolean isPasswordValid(String password);

    boolean isLoginValid(String login);

    boolean isUserRequestValid(Long userId);
}
