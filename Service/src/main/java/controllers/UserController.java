package controllers;

import dto.Dto;
import dto.EmployeeDto;
import dto.FullUserDto;
import dto.UserDto;
import entity.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class UserController {

    @Autowired
    private static final Logger logger = LogManager.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Get all users", nickname = "UserController.getAllUsers")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Users geted ok")})
    @GetMapping("/users")
    @Dto(UserDto.class)
    @Secured({"ROLE_HR", "ROLE_ACCOUNTANT"})
    public Page<User> getAllUsers(Pageable pageable) {
        logger.debug("Get all users");
        return userService.getAllUsers(pageable);
    }

    @ApiOperation(value = "Get users by id", nickname = "UserController.getUserById")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User geted ok")})
    @GetMapping("/users/{userId}")
    @Dto(UserDto.class)
    @Secured("ROLE_HR")
    public User getUserById(@PathVariable(value = "userId") Long userId) {
        logger.debug("Get user with id: " + userId);
        return userService.getUserById(userId);
    }

    @ApiOperation(value = "Create new user", nickname = "UserController.createUser")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "User created ok")})
    @PostMapping("/users")
    @Dto(FullUserDto.class)
    @Secured({"ROLE_HR", "ROLE_ACCOUNTANT"})
    public User createUser(@Valid @RequestBody User user) {
        logger.debug("Create new user ");
        return userService.createUser(user, user.getEmployee());
    }

    @ApiOperation(value = "Update user", nickname = "UserController.updateUser")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User updated ok")})
    @PutMapping("/users/{userId}")
    @Dto(FullUserDto.class)
    @Secured("ROLE_HR")
    public User updateUser(@PathVariable Long userId, @Valid @RequestBody User userRequest) {
        logger.debug("Update user with id: " + userId);
        return userService.updateUser(userId, userRequest);
    }

    @ApiOperation(value = "Delete user", nickname = "UserController.deleteUser")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User deleted ok")})
    @DeleteMapping("/users/{userId}")
    @Dto(UserDto.class)
    @Secured({"ROLE_HR", "ROLE_ACCOUNTANT"})
    public ResponseEntity<?> deleteUser(@PathVariable Long userId) {
        logger.debug("delete user with id: " + userId);
        return userService.deleteUser(userId);
    }

    @ApiOperation(value = "Get user subordinates", nickname = "UserController.getUserSubordinates")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Users geted ok")})
    @GetMapping("/users/{userId}/subordinates")
    @Dto(UserDto.class)
    @Secured({"ROLE_HR", "ROLE_USER"})
    public Page<User> getUserSubordinates(@PathVariable(value = "userId") Integer userId, Pageable pageable) {
        logger.debug("get subordinates of user with id: " + userId);
        return userService.getUserSubordinates(userId, pageable);
    }

    @ApiOperation(value = "Get users without head", nickname = "UserController.getUsersWithoutHead")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Users geted ok")})
    @GetMapping("/users/without_head")
    @Dto(UserDto.class)
    @Secured("ROLE_HR")
    public Page<User> getUsersWithoutHead(Pageable pageable) {
        logger.debug("get user without head");
        return userService.getAllUsersWithoutHead(pageable);
    }

    @ApiOperation(value = "Add subordinate to users", nickname = "UserController.addSubordinateToUser")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Subordinate added ok")})
    @GetMapping("/users/{userId}/subordinates/{subordinateId}")
    @Dto(UserDto.class)
    @Secured("ROLE_HR")
    public User addSubordinateToUser(@PathVariable(value = "userId") Long userId,
                                     @PathVariable(value = "subordinateId") Long subordinateId) {
        logger.debug("add subordinate with id: "+subordinateId+" to user with id: " + userId);
        return userService.addSubordinateToUser(userId, subordinateId);
    }

    @ApiOperation(value = "Delete subordinate from users", nickname = "UserController.deleteSubordinateFromUser")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Subordinate  deleted ok")})
    @DeleteMapping("/users/{userId}/subordinates/{subordinateId}")
    @Dto(UserDto.class)
    @Secured("ROLE_HR")
    public User deleteSubordinateFromUser(@PathVariable(value = "userId") Long userId,
                                          @PathVariable(value = "subordinateId") Long subordinateId) {
        logger.debug("delete subordinate with id: "+subordinateId+" from user with id: " + userId);
        return userService.deleteSubordinateFromUser(userId, subordinateId);
    }

    @ApiOperation(value = "Set hours to subordinate", nickname = "UserController.setHoursToSubordinate")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Hours set ok")})
    @PutMapping("/users/{userId}/subordinates/{subordinateId}/hours")
    @Dto(UserDto.class)
    @Secured("ROLE_USER")
    public User setHoursToSubordinate(@PathVariable(value = "userId") Long userId,
                                      @PathVariable(value = "subordinateId") Long subordinateId,
                                      @RequestBody Integer hours) {
        logger.debug("add "+hours+" hours"+ "to subordinate with id: "+subordinateId+" from user with id: " + userId);
        return userService.setHoursForSubordinate(userId, subordinateId, hours);
    }

    @ApiOperation(value = "Change users password", nickname = "UserController.changeUserPassword")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Users geted ok")})
    @PutMapping("/users/{userId}/change_password")
    @Secured("ROLE_USER")
    @Dto(FullUserDto.class)
    public User changeUserPassword(@PathVariable(value = "userId") Long userId, @RequestBody String password) {
        logger.debug("change password of  user with id: " + userId);
        return userService.changeUserPassword(userId, password);
    }

    @ApiOperation(value = "Change user login", nickname = "UserController.changeUserLogin")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Login changed ok")})
    @PutMapping("/users/{userId}/change_login")
    @Secured("ROLE_USER")
    @Dto(FullUserDto.class)
    public User changeUserLogin(@PathVariable(value = "userId") Long userId, @RequestBody String login) {
        logger.debug("change login of  user with id: " + userId);
        return userService.changeUserLogin(userId, login);
    }

    @ApiOperation(value = "Calculate user salary", nickname = "UserController.calculateSalary")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Salary calculated ok")})
    @GetMapping("/users/{userId}/calculate_salary")
    @Dto(UserDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public User calculateSalary(@PathVariable(value = "userId") Long userId) {
        logger.debug("calculate salary for user with id: " + userId);
        return userService.calculateSalary(userId);
    }




}
