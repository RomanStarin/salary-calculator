package controllers;

import dto.Dto;
import dto.TaxDto;
import entity.Tax;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import service.TaxService;

import javax.validation.Valid;

@RestController
public class TaxController {

    @Autowired
    private static final Logger logger = LogManager.getLogger(TaxController.class);

    @Autowired
    TaxService taxService;

    @ApiOperation(value = "Get all taxes", nickname = "TaxController.getAllTaxes")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Taxes geted ok")})
    @GetMapping("/taxes")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Tax> getAllTaxes(Pageable pageable) {
        logger.debug("get all taxes");
        return taxService.getAllTaxes(pageable);
    }

    @ApiOperation(value = "Get all user taxes", nickname = "TaxController.getAllUserTaxes")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User taxes geted ok")})
    @GetMapping("/users/{userId}/taxes")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Tax> getAllTaxesByUserId(@PathVariable(value = "userId") Long userId,
                                         Pageable pageable) {
        logger.debug("get taxes of user with id: " + userId);
        return taxService.getAllTaxesByUserId(userId, pageable);
    }


    @ApiOperation(value = "Get all tax by userId", nickname = "TaxController.getTaxById")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Tax geted ok")})
    @GetMapping("/taxes/{taxId}")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Tax getTaxById(@PathVariable(value = "taxId") Long taxId) {
        logger.debug("get tax with id: " +taxId);
        return taxService.getTaxById(taxId);
    }

    @ApiOperation(value = "Create new tax", nickname = "TaxController.createTax")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Taxes created ok")})
    @PostMapping("/users/{userId}/taxes")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Tax createTax(@PathVariable(value = "userId") Long userId, @Valid @RequestBody Tax tax) {
        logger.debug("create new tax");
        return taxService.createTax(userId, tax);
    }

    @ApiOperation(value = "Update tax", nickname = "TaxController.updateTax")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Taxes updated ok")})
    @PutMapping("users/{userId}/taxes/{taxId}")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Tax updateTax(@PathVariable(value = "userId") Long userId,
                         @PathVariable(value = "taxId") Long taxId,
                         @Valid @RequestBody Tax taxToUpdate) {
        logger.debug("update tax with id: " + taxId);
        return taxService.updateTax(userId, taxId, taxToUpdate);
    }

    @ApiOperation(value = "Delete tax", nickname = "TaxController.deleteTax")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Taxes deleted ok")})
    @DeleteMapping("users/{userId}/taxes/{taxId}")
    @Dto(TaxDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public ResponseEntity<?> deleteTax(@PathVariable(value = "userId") Long userId,
                                       @PathVariable(value = "taxId") Long taxId) {
        logger.debug("delete tax with id: " + taxId);
        return taxService.deleteTax(userId, taxId);
    }


}
