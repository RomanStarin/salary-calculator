package controllers;

import dto.Dto;
import dto.FineDto;
import entity.Fine;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import service.FineService;


import javax.validation.Valid;

@RestController
public class FineController {

    @Autowired
    private static final Logger logger = LogManager.getLogger(FineController.class);

    @Autowired
    FineService fineService;

    @ApiOperation(value = "Get all fines", nickname = "FineController.getAllFines")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fines geted ok")})
    @GetMapping("/fines")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Fine> getAllFines(Pageable pageable) {
        logger.debug("get all fines");
        return fineService.getAllFines(pageable);
    }

    @ApiOperation(value = "Get all user fines", nickname = "FineController.getAllUserFines")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fines geted ok")})
    @GetMapping("/users/{userId}/fines")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Fine> getAllFinesByUserId(@PathVariable(value = "userId") Long userId,
                                          Pageable pageable) {
        logger.debug("get all fines of user with id: " + userId);
        return fineService.getAllFinesByUserId(userId, pageable);
    }

    @ApiOperation(value = "Get  fine by fineId", nickname = "FineController.getFineById")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fine geted ok")})
    @GetMapping("/fines/{fineId}")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Fine getFineById(@PathVariable(value = "FineId") Long fineId) {
        logger.debug("get fine with id: " + fineId);
        return fineService.getFineById(fineId);
    }


    @ApiOperation(value = "Create new fine", nickname = "FineController.createFine")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Fine created")})
    @PostMapping("/users/{userId}/fines")
    @Dto(FineDto.class)
    @Secured("ROLE_USER")
    public Fine createFine(@PathVariable(value = "userId") Long userId, @Valid @RequestBody Fine fine) {
        logger.debug("create new fine");
        return fineService.createFine(userId, fine);
    }

    @ApiOperation(value = "Update fine", nickname = "FineController.updateFine")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fine updated")})
    @PutMapping("users/{userId}/fines/{fineId}")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Fine updateFine(@PathVariable(value = "userId") Long userId,
                           @PathVariable(value = "fineId") Long fineId,
                           @Valid @RequestBody Fine fineToUpdate) {
        logger.debug("update fine with id: " + fineId);
        return fineService.updateFine(userId, fineId, fineToUpdate);
    }

    @ApiOperation(value = "Delete fine", nickname = "FineController.deleteFine")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fine deleted")})
    @DeleteMapping("users/{userId}/fines/{fineId}")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public ResponseEntity<?> deleteFine(@PathVariable(value = "userId") Long userId,
                                        @PathVariable(value = "fineId") Long fineId) {
        logger.debug("delete fine with id: " + fineId);
        return fineService.deleteFine(userId, fineId);
    }

    @ApiOperation(value = "Get user subordinate fines", nickname = "FineController.getSubordinateFines")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fines geted ok")})
    @GetMapping("/users/{userId}/subordinates/{subordinateId}/fines")
    @Dto(FineDto.class)
    @Secured({"ROLE_ACCOUNTANT", "ROLE_USER"})
    public Page<Fine> getSubordinateFines(@PathVariable(value = "userId") Long userId,
                                          @PathVariable(value = "subordinateId") Long subordinateId, Pageable pageable) {
        logger.debug("get fines of subordinate with id: " + subordinateId);
        return fineService.getAllFinesByUserId(subordinateId, pageable);
    }

    @ApiOperation(value = "Get unapproved fines", nickname = "FineController.getUnapprovedFines")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fines geted ok")})
    @GetMapping("/fines/unapproved")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Fine> getUnapprovedFines(Pageable pageable) {
        logger.debug("get unapproved fines");
        return fineService.getUnapprovedFines(pageable);
    }

    @ApiOperation(value = "Approve user fine", nickname = "FineController.approveFine")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Fine approved")})
    @GetMapping("users/{userId}/fines/{fineId}/approve")
    @Dto(FineDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Fine approveFine(@PathVariable(value = "userId") Long userId,
                            @PathVariable(value = "fineId") Long fineId) {
        logger.debug("approve fine with id: " + fineId);
        return fineService.approveFine(userId, fineId);
    }
}
