package controllers;

import dto.Dto;
import dto.PrizeDto;
import entity.Prize;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import service.PrizeService;

import javax.validation.Valid;

@RestController
public class PrizeController {

    @Autowired
    private static final Logger logger = LogManager.getLogger(PrizeController.class);

    @Autowired
    PrizeService prizeService;

    @ApiOperation(value = "Get all prizes", nickname = "PrizeController.getAllPrizes")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prizes geted ok")})
    @GetMapping("/prizes")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Prize> getAllPrizes(Pageable pageable) {
        logger.debug("get all prizes");
        return prizeService.getAllPrizes(pageable);
    }

    @ApiOperation(value = "Get all user prizes", nickname = "PrizeController.getAllUserPrizes")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "User prizes geted ok")})
    @GetMapping("/users/{userId}/prizes")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Prize> getAllPrizesByUserId(@PathVariable(value = "userId") Long userId,
                                         Pageable pageable) {
        logger.debug("get prizes of user with id: " +userId);
        return prizeService.getAllPrizesByUserId(userId, pageable);
    }

    @ApiOperation(value = "Get prizes by userId", nickname = "PrizeController.getPrizeById")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prize geted ok")})
    @GetMapping("/prizes/{prizeId}")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Prize getPrizeById(@PathVariable(value = "prizeId") Long prizeId) {
        logger.debug("get prize with id: " +prizeId);
        return prizeService.getPrizeById(prizeId);
    }

    @ApiOperation(value = "Create new prize", nickname = "PrizeController.createPrize")
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Prize created")})
    @PostMapping("/users/{userId}/prizes")
    @Dto(PrizeDto.class)
    @Secured("ROLE_USER")
    public Prize createPrize(@PathVariable(value = "userId") Long userId, @Valid @RequestBody Prize prize) {
        logger.debug("create new prize");
        return prizeService.createPrize(userId, prize);
    }

    @ApiOperation(value = "Update prize", nickname = "PrizeController.updatePrize")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prize updated")})
    @PutMapping("users/{userId}/prizes/{prizeId}")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Prize updatePrize(@PathVariable(value = "userId") Long userId,
                         @PathVariable(value = "prizeId") Long prizeId,
                         @Valid @RequestBody Prize prizeToUpdate) {
        logger.debug("update prize with id: " + prizeId);
        return prizeService.updatePrize(userId, prizeId, prizeToUpdate);
    }

    @ApiOperation(value = "Delete prize", nickname = "PrizeController.deletePrize")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prize deleted")})
    @DeleteMapping("users/{userId}/prizes/{prizeId}")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public ResponseEntity<?> deletePrize(@PathVariable(value = "userId") Long userId,
                                       @PathVariable(value = "prizeId") Long prizeId) {
        logger.debug("delete prize with id: " +prizeId);
        return prizeService.deletePrize(userId, prizeId);
    }

    @GetMapping("/users/{userId}/subordinates/{subordinateId}/prizes")
    @Dto(PrizeDto.class)
    @Secured({"ROLE_ACCOUNTANT","ROLE_USER"})
    public Page<Prize> getSubordinatePrizes(@PathVariable (value = "userId") Long userId,
                                            @PathVariable (value = "subordinateId") Long subordinateId, Pageable pageable){
        logger.debug("get prizes of user with id: " + userId);
        return prizeService.getAllPrizesByUserId(subordinateId, pageable);
    }

    @ApiOperation(value = "Get unapproved prizes", nickname = "PrizeController.getUnapprovedPrizes")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prizes geted ok")})
    @GetMapping("/prizes/unapproved")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Page<Prize> getUnapprovedPrizes(Pageable pageable){
        logger.debug("get unapproved prizes");
        return prizeService.getUnapprovedPrizes(pageable);
    }

    @ApiOperation(value = "Approve prize", nickname = "PrizeController.approvePrize")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Prize approved")})
    @GetMapping("users/{userId}/prizes/{prizeId}/approve")
    @Dto(PrizeDto.class)
    @Secured("ROLE_ACCOUNTANT")
    public Prize approvePrize(@PathVariable(value = "userId") Long userId,
                              @PathVariable(value = "prizeId") Long prizeId){
        logger.debug("approve prize with id: " +prizeId);
        return prizeService.approvePrize(userId, prizeId);
    }
}
