package salaryCalculator;


import controllers.UserController;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import repository.EmployeeRepository;
import repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EntityScan("entity")
@EnableJpaRepositories("repository")
@ComponentScan({"controllers", "service", "utils", "config", "dto", "security"})
@SpringBootApplication()
public class SalaryCalculatorApplication implements CommandLineRunner {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}



	public static void main(String[] args) {
		SpringApplication.run(SalaryCalculatorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
