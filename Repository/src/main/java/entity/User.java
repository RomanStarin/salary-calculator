package entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "users")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @NotNull
    @Column(name = "login")
    private String login;

    @NonNull
    @NotNull
    @Column(name = "password")
    private String password;

    @NonNull
    @NotNull
    @Size(max = 65)
    @Column(name = "name")
    private String name;

    @NonNull
    @Size(max = 65)
    @Column(name = "surname")
    private String surname;

    @NonNull
    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Role role;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
    @JsonManagedReference
    private Employee employee;


}
