package entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
public class Salary {

    @Id
    private Long id;
    private double salaryBeforeTaxes = 0.0d;
    private double salaryAfterTaxes = 0.0d;
    private double prizesAmount = 0.0d;
    private double finesAmount = 0.0d;
    private double taxesAmount = 0.0d;
}
