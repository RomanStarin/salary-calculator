package entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Entity
@Table(name = "employees")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @Size(max = 100)
    private String position;

    @NonNull
    @NotNull
    @Email
    @Size(max = 100)
    @Column(unique = true)
    private String email;

    @NonNull
    @Column(precision = 16, scale = 2, name = "salary_per_hour")
    private double salaryPerHour;

    @NonNull
    @Column(precision = 16, scale = 2)
    private double balance;

    @NonNull
    @Column(precision = 11, scale = 0, name = "hours_per_month")
    private int hoursPerMonth;


    @Column(precision = 11, scale = 0, name = "head_id")
    private int headId;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @JsonBackReference
    private User user;


}
