package entity;

import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "fines")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Fine implements MotivationActivity, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @NotNull
    @Size(max = 65)
    @Column(name = "cause")
    private String cause;

    @NonNull
    @Column(precision = 16, scale = 2)
    private double amount;

    @NonNull
    @Column(nullable = false, columnDefinition = "TINYINT(1)")
    private boolean approved;

    @NonNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    @JsonProperty("user_id")
    private User user;

    public Fine(long id, @NonNull @NotNull @Size(max = 65) String cause, @NonNull double amount, @NonNull boolean approved) {
        this.id = id;
        this.cause = cause;
        this.amount = amount;
        this.approved = approved;
    }
}
