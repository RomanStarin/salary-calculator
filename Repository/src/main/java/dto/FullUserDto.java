package dto;

public class FullUserDto {

    private Long id;
    private String login;
    private String password;
    private String name;
    private String surname;
    private Long employeeId;
    private String employeePosition;
    private String employeeEmail;
    private double employeeSalaryPerHour;
    private double employeeBalance;
    private int employeeHoursPerMonth;
    private int employeeHeadId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeePosition() {
        return employeePosition;
    }

    public void setEmployeePosition(String employeePosition) {
        this.employeePosition = employeePosition;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public double getEmployeeSalaryPerHour() {
        return employeeSalaryPerHour;
    }

    public void setEmployeeSalaryPerHour(double employeeSalaryPerHour) {
        this.employeeSalaryPerHour = employeeSalaryPerHour;
    }

    public double getEmployeeBalance() {
        return employeeBalance;
    }

    public void setEmployeeBalance(double employeeBalance) {
        this.employeeBalance = employeeBalance;
    }

    public int getEmployeeHoursPerMonth() {
        return employeeHoursPerMonth;
    }

    public void setEmployeeHoursPerMonth(int employeeHoursPerMonth) {
        this.employeeHoursPerMonth = employeeHoursPerMonth;
    }

    public int getEmployeeHeadId() {
        return employeeHeadId;
    }

    public void setEmployeeHeadId(int employeeHeadId) {
        this.employeeHeadId = employeeHeadId;
    }
}
