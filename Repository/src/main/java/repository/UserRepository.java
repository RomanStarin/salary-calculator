package repository;

import entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "select * from users join employees where users.id = employees.id and employees.head_id = ?1", nativeQuery = true)
    Page<User> findByHeadId(Integer headId, Pageable pageable);

    @Query(value = "select * from users join employees where users.id = employees.id and employees.head_id = 0", nativeQuery = true)
    Page<User> getUsersWithoutHead(Pageable pageable);

    User findByLogin(String login);
}
