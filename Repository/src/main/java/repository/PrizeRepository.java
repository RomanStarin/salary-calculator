package repository;

import entity.Prize;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PrizeRepository extends JpaRepository<Prize, Long> {

    Page<Prize> findByUserId(Long userId, Pageable pageable);

    Optional<Prize> findByIdAndUserId(Long id, Long userId);

    @Query(value = "select * from prizes where prizes.approved = false ", nativeQuery = true)
    Page<Prize> getUnapprovedPrizes(Pageable pageable);
}
