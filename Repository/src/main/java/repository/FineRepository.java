package repository;

import entity.Fine;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FineRepository extends JpaRepository<Fine, Long> {

    Page<Fine> findByUserId(Long userId, Pageable pageable);

    Optional<Fine> findByIdAndUserId(Long id, Long userId);

    @Query(value = "select * from fines where fines.approved = false ", nativeQuery = true)
    Page<Fine> getUnapprovedFines(Pageable pageable);
}
