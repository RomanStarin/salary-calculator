package repository;

import entity.Tax;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaxRepository extends JpaRepository<Tax, Long> {

    Page<Tax> findByUserId(Long userId, Pageable pageable);

    Optional<Tax> findByIdAndUserId(Long id, Long userId);

    List<Tax> findByUserId(Long userId);

}
